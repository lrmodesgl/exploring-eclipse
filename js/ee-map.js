
// immediately invoking openlayer map
(function (){
    
    new ol.Map({
	layers: [
	    new ol.layer.Tile({source: new ol.source.OSM()})
	],
	view: new ol.View({
	    center: [12, 79],
	    zoom: 2
	}),
	target: 'omap'
    });
})();
